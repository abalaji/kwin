# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kwin package.
# Tommi Nieminen <translator@legisign.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-09 00:39+0000\n"
"PO-Revision-Date: 2023-12-11 18:05+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/main.qml:32
#, fuzzy, kde-format
#| msgid ""
#| "Legacy X11 apps require the ability to read keystrokes typed in other "
#| "apps for features that are activated using global keyboard shortcuts. "
#| "This is disabled by default for security reasons. If you need to use such "
#| "apps, you can choose your preferred balance of security and functionality "
#| "here."
msgid ""
"Some legacy X11 apps require the ability to read keystrokes typed in other "
"apps for certain features, such as handling global keyboard shortcuts. This "
"is allowed by default. However other features may require the ability to "
"read all keys, and this is disabled by default for security reasons. If you "
"need to use such apps, you can choose your preferred balance of security and "
"functionality here."
msgstr ""
"Vanhat X11-sovellukset vaativat luvan nähdä muiden sovellusten voidakseen "
"aktivoida toimintojaan yleispikanäppäimin. Tietoturvasyistä tätä ei yleensä "
"sallita. Jos käytät tällaisia sovelluksia, voit tässä tasapainottaa "
"tietoturvaa ja toiminnallisuutta haluamallasi tapaa."

#: ui/main.qml:48
#, kde-format
msgid "Allow legacy X11 apps to read keystrokes typed in all apps:"
msgstr ""
"Salli vanhojen X11-sovellusten lukea kaikkien sovellusten "
"näppäinpainallukset:"

#: ui/main.qml:49
#, kde-format
msgid "Never"
msgstr "Ei koskaan"

#: ui/main.qml:55
#, fuzzy, kde-format
#| msgid "Only Meta, Control, Alt, and Shift keys"
msgid "Only Meta, Control, Alt and Shift keys"
msgstr "Vain Meta-, Ctrl-, Alt- ja vaihtonäppäimet"

#: ui/main.qml:61
#, kde-format
msgid ""
"As above, plus any key typed while the Control, Alt, or Meta keys are pressed"
msgstr ""
"Kuten yllä sekä mikä tahansa näppäin, jos Ctrl-, Alt- tai Meta-näppäintä "
"painetaan"

#: ui/main.qml:68
#, kde-format
msgid "Always"
msgstr "Aina"

#: ui/main.qml:78
#, kde-format
msgid "Additionally include mouse buttons"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."
msgstr ""
"Huomaa, että asetuksen käyttö heikentää järjestelmän tietoturvan X11-"
"istunnon tasolle, jolloin pahantahtoiset ohjelmat voivat varastaa salasanan "
"ja nähdä kirjoitetun tekstin. Varmista, että ymmärrät ja hyväksyt riskin."

#~ msgid "Only non-character keys"
#~ msgstr "Vain ei-merkkinäppäimet"

#~ msgid ""
#~ "This module lets configure which keyboard events are forwarded to X11 "
#~ "apps regardless of their focus."
#~ msgstr ""
#~ "Tässä moduulissa voi asettaa, mitkä näppäimistötapahtumat kohdistuksesta "
#~ "riippumatta välitetään X11-sovelluksiin."
